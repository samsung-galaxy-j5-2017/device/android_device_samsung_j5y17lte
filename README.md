Device configuration for the Samsung Galaxy J5 Pro

- Copyright (C) 2017 The LineageOS Project
- Copyright (C) 2017 Siddhant Naik
- Copyright (C) 2018 Exynos7870
- Copyright (C) 2018 AnanJaser
- Copyright (C) 2019 steadfasterX
- Copyright (C) 2018 Maru OS Project
- Copyright (C) 2020 rbnyellow

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.